//
//  InterfaceController.swift
//  RaptorsBasktballApp WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    //MARK: Data Source
    var gameList:[BasketballGame] = []
    
    @IBOutlet weak var gameTable: WKInterfaceTable!
    
    
    func createGameObjects() {
        let d = Date()
        //let g1 = BasketballGame(team1: "Trail Blazers", team2: "Raptors", location: "Toronto", startTime: d)
        let g2 = BasketballGame(team1: "Raptors", team2: "Pistons", location: "Detroit", startTime: d)
        let g3 = BasketballGame(team1: "Rockets", team2: "Raptors", location: "Toronto", startTime: d)
        let g4 = BasketballGame(team1: "Raptors", team2: "Pelicans", location: "New Orleans", startTime: d)
        
        //gameList.append(g1)
        gameList.append(g2)
        gameList.append(g3)
        gameList.append(g4)
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.createGameObjects()
        
        // MARK: Populate your tableview with data
        // ------------------
        // 0. tell IOS how many rows your table should have
        self.gameTable.setNumberOfRows(self.gameList.count, withRowType:"myRow")
        
        
        print("number of rows: \(self.gameList.count)")
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, game) in self.gameList.enumerated() {
            
            
            let row = self.gameTable.rowController(at: i) as! BasketballRowController
            
            
            
            
            row.team1Label.setText(game.team1!)
            row.team2Label.setText(game.team2!)
            row.locationLabel.setText(game.location!)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm"
            let dateString = dateFormatter.string(from: game.startTime!)
            row.startTimeLabel.setText(dateString)
            
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
