//
//  BasketballGame.swift
//  RaptorsBasktballApp
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit

class BasketballGame: NSObject {
    // MARK: class properties
    var team1:String?
    var team2:String?
    var location:String?
    var startTime:Date?
    
    // MARK: contructor
    convenience override init() {
        let d = Date()
        self.init(team1:"", team2:"", location:"", startTime: d)
    }
    
    init(team1:String, team2:String, location:String, startTime:Date) {
        self.team1 = team1
        self.team2 = team2
        self.location = location
        self.startTime = startTime
    }
}
